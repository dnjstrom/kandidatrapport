\chapter{Introduction}
\pagenumbering{arabic}

As smartphones continue to grow in power and screen size, the variety and scope of tasks that users wish to accomplish grows as well. Mobile e-book reading, once the domain of large-screen dedicated reader devices, is becoming feasible on smartphones as modern devices often feature screen sizes in excess of 3.5" \citep{PhoneSize2012}. Still, the available screen real-estate is rather limited and navigation through complex documents can feel frustrating and restrictive.

If workspace area is the Achilles' heel of the smartphone e-reader, then mobility is one of its greatest advantages. While carrying only as much as one or two physical books might require a backpack, e-books enables one to carry an entire library in a pocket. This mobility, coupled with other benefits such as search and annotations, makes the option to read on your phone very desirable in many scenarios.

Currently, a number of dedicated e-book reading devices are available in the market. Such devices often provide a strong reading experience, but they are rarely inexpensive and often cumbersome. It is likely cheaper and more convenient to download an application for those that already own and carry a smartphone.  

Simple text-only fiction is not overly challenging to present in a user-friendly manner even on a very small screen. Documents of greater complexity and user interaction such as textbooks are another matter; they often contain figures, tables and reference schemes. Reading such a text for its intended purpose can entail going back and forth through the document, re-reading sections, cross referencing facts and consulting tables, diagrams and indices. 

Existing e-reader applications for Android often focus on bigger devices such as tablets, or fail to provide strong support for complex texts. This project explores design ideas that can be used to provide rich interaction with complex books on small screens, while remaining easy-to-use and accessible.

\section{Background}

\begin{bgList}[H]
\begin{description} 
\item[1971] The first e-book was created by Michael Hart on the 4th of July as the initiation of Project Gutenberg, the first digital ``library''.
\item[1974] The Internet is born with the introduction of TCP/IP and the first recorded use of the word ``Internet'' \citep{RfcTcpIp}.
\item[1991] The Sony Data Discman, an early predecessor to modern e-readers, became available for purchase \citep{Lawinski2010}.
\item[1993] A combined phone-pager-fax-pda gadget from IBM, called the Simon PDA, is released in stores. The device is even equipped with a touchscreen. \mbox{\citep{TC1994}}.
\item[1994] The first library website went live.
\item[1995] Amazon.com launched as the first online bookstore. Only the store was digital, not the books.
\item[1997] Many newspapers were publishing content online.
\item[1999] The Open Ebook format was created by the IDPF.
\item[2000] Project Gutenberg reached 1000 e-books and Amazon opened its digital book store with the same number of books.
\item[2002] The Blackberry was introduced to the market as a phone that could also be used for email and web browsing \citep{Reed2010}.
\item[2006] Google Books was launched with the goal of providing digitized and searchable books from partner libraries.
\item[2007] The Open Publication Structure, also known as \emph{EPUB}, supersedes the Open eBook format.
\item[2007] Amazon's Kindle, one of the best known e-readers, becomes available on Amazon.com \citep{Lawinski2010}.
\item[2007] The iPhone is announced \citep{Apple2007} and redefines the concept of what a smartphone is.
\item[2010] The iPad is released \citep{Lawinski2010} along with iBooks and the iBookstore.
\end{description}

\caption{Unless otherwise noted, facts are gathered from \cite{Lebert2008}.}
\end{bgList}

It was recognized early in the development of computers that text was a nice input format for bridging the gap between humans and computers. Consequently, keyboards and the ASCII character encoding standard were early creations in the history of computers. At the time, most computers were used as advanced programmable calculators for time-consuming numerical calculations, but it would not be long until their aptitude for text processing was realized. The first e-book was created in 1971 when Michael Hart typed the text of the ``The United States Declaration of Independence'' into a mainframe at his university, storing it in a simple text file. Two years later what was arguably the first PC was released. The groundbreaking Xerox Alto was designed specifically for document processing and printing \citep{Thacker1979}.

The first e-books were mainly technical manuals and short documents for a limited audience. Reading on a computer screen at the time might not have been the most pleasant experience and the size of the computers made reading on them far less flexible than reading traditional books. The ease of distribution introduced by the wide adoption of the Internet made e-books more attractive but could not make up for the downsides. An effort to change the clumsiness of e-reading was made in 1991 with the Sony Data Discman but it was not enough to sway the masses. In 1998 NuvoMedia's Rocket eBook, a fairly capable e-reader even by the standards of today made another attempt but the market was not ready \citep{Lawinski2010}. E-reading only became somewhat widely adopted in 2007 with the release of the Amazon Kindle, but was not an overnight success. While the the Kindle was a capable device, it was unable to overcome perceptions of e-reading as something for ``techies'' or ``nerds'', which deterred some from investing in the product.

Since the release of the iPad in 2010, carrying a tablet is both socially accepted and even a status symbol. Tablets have quickly become a natural way for accessing web content, writing notes and checking e-mail. With the introduction of services such as iBooks and iBooks Author there are reasons to believe that the normal way of creating and consuming books could change considerably. 


\section{Purpose}
The goal of this bachelor's project is to explore smartphone interaction design ideas for e-reading, and to develop a prototype that exemplifies these ideas. This prototype is named 3Book, and is intended to continue evolving after the completion of the project.

The long-term vision for the 3Book application is to provide a reader that meets many of the requirements of a college student reading environment while limiting the need to carry physical copies of books. This thesis aims to explain the research and reasoning behind the design principles used in the application.

The vision for the application has been stable throughout the project while the focus of the project and this thesis has been more fluid. Some time was spent researching alternate goals, which is discussed in \autoref{sec:abandoned}.


\subsection{Abandoned thesis aims} \label{sec:abandoned}

The idea of supporting advanced mathematical formulae written in a structured language was brought up in early discussions with the project supervisor. Supporting formulae fit nicely into our idea of making a reader that is useful in an academic setting. The fact that the chosen format (see the format discussion in \autoref{subsec:formats}) supports MathML made the idea even more attractive. However, it was found that creating a rendering engine for mathematics was too big of an undertaking, especially as pre-rendered images were found to be an acceptable as well as the prevalent solution for displaying math in books. Considering that MathML is a W3C recommendation it might be a worthwhile effort to implement support for the language at a later time.

Displaying musical notation was also investigated as a possible focus of the thesis. The idea was considered interesting and challenging as sheet music often run into space limitations even in print, to say nothing of the difficulty of displaying it on a small screen in a usable manner. The aim was to limit the need of bringing physical music sheets to practice sessions and study. The concept was found both unique and very engaging, but the scope would require a project of its own. As music is only tangentially related to the overall aim of the project, this idea was discarded.
 
In the end, while there were many interesting smartphone interaction design angles, it was considered most prudent to first work on an application prototype that could form a base for later undertakings.

\section{Method}
The e-reader application was partially developed under a SCRUM-like agile regimen. At the outset of development, a list of programming tasks was drawn up, estimated and prioritized. This resulted in a document which specified the programming effort required to implement the e-reader application. High-priority tasks were each assigned to a theme, and subsequently the themes were slotted into one of the available three three-week iterations that were planned. 

The original plan was later found lacking in many respects as new information and knowledge was discovered. As time went by the actual work being done started to diverge sharply from the plan. 
 A much better sense of what was required in terms of programming had been achieved midway through the project, and a new list was then drawn up. This plan was made on the basis of far more domain knowledge and was followed throughout the rest of the project.
 
Since there was only three project members, a rigid division of labor was never formulated, though each member focused on tasks appropriate to their specific knowledge and experience. The limited workforce and 15 week time span of the project severely restricted what was considered feasible to achieve in terms of a complete product.

\subsection{Interface design methodology}
The interaction design concepts presented in this thesis were developed through iterative prototyping.
First the real-life situations where the application would be used was considered and the solutions used in existing applications analyzed. The analytic process can be summed up as asking four questions: 

\begin{itemize}
  \item How did other applications approach the problem?
  \item What are the issues with their solutions?
  \item How could their solutions be improved?
  \item What features are missing from their solutions?
\end{itemize}

Sketches of various solutions were then made, to provide a better understanding of how the solutions would look in a real application. Figure \ref{fig:SketchDrilldown} shows an early concept sketch for the navigation scroll detailed later in this report. Unimplemented ideas like content preview and bookmarks are visible in the sketch. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{img/scetch_drilldown.png}
    \caption{Sketch of book navigation interface.}
    \label{fig:SketchDrilldown}
\end{figure}

The sketches were then discussed and improved further. Once they had reached a decent level of quality they were translated into computer graphics using an image editing program. These concepts were then given an elementary implementation in code to test their functionality on a real device. Concepts that did not function as intended or were shown to be bad design decisions were either further refined and developed or else discarded.  
  
