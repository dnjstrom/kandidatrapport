\chapter{Implementation details}
\label{chap:Implementation}
A large portion of the time invested in this project has been put towards creating a prototype application as a ``proof of concept'' of our interaction design ideas. This chapter describes implementation details that were critical to the creation of the prototype.

\section{Format considerations}
One of the main arguments for mobile e-book readers is the convenience inherent in being able to carry an entire library in one's pocket. This vision of ``one reader, infinite books'' is severely compromised by the myriad of e-book formats that are in use\footnote{No less than 17 e-book formats are listed on Wikipedia at the time of writing.}. This fragmentation is caused by publishers and e-book vendors who often only make their texts available in a small range of formats or even a single format, which means that the avid e-book user must keep a stable of several reading devices and applications. A strong e-book reader application should provide support for as many formats as possible in order to provide greater user convenience.

Supporting multiple formats in order to present a unified reading experience is a goal of the final product, but not necessarily of the prototype. For this reason the code behind the prototype is focused on handling the single format, but designed for future format extensions. This approach means that all formats that provide a minimum degree of functionality, mainly related to structure, can be implemented.


\subsection{Format requirements and design space} \label{subsubsec: format requirements}
In choosing a file format one must consider more than simple ease of implementation. The choice of file format strongly constrains the design space available as different file formats provide different feature sets. This means that the choice of format used in the application must be based on the requirements derived from the design concepts.

Making a strict definition of a page size or shape was considered detrimental to the reading experience, due to the varied screen sizes of Android devices. Therefore it was decided that the chosen format had to be structured without the notion of pages, so that its contents could be ``re-flown'' for each screen. 

Presenting the information in such a manner, appropriate to each specific device, requires that the format provides a facility for separating styling from content. The presentation is further enhanced when the format exposes semantic information, as it is then easy to differentiate between distinct elements such as headings, images and body text. 

Another major requirement was access to the structure of the text. This would support quick navigation between different parts of the document. Ideally, the format should describe several levels of structure so that chapters, sections, sections etc. are all accessible in an easy manner. There was also a desire to transcend the limitations of the physical book by using the full capabilities of the smartphone. Alongside text and images, an author should be able to include audio and video content. This could be especially useful for educational textbooks.

Finally, the ideal format should be an open industry standard, utilizing technologies that are simple and well-studied. Formats based on technologies that the project members had experience with were also preferred, given the narrow time constraints of the project.
    

\subsection{Results of format pre-study} \label{subsec:formats}
A survey was done of the popular e-book formats where the features of the formats were assessed in relation to the requirements discussed above. EPUB was consequently chosen as the format supported by the prototype, as it met all of the requirements. It is an open standard maintained by the International Digital Publishing Forum (IDPF) and based on standard web technologies such as HTML and CSS. This means a clear division between content and styling is present in the format. The latest version of EPUB (EPUB 3) also makes use of new HTML5 and CSS3 features to support the embedding of video and audio, among other advanced features. 
 
The forgiving navigation concept presented in \autoref{subsec:forgivingNavigation} carries close ties to ease of use and accessibility and EPUB 3 supports many accessibility features through the integration of the Digital Accessible Information SYstem (DAISY). Finally, EPUB is currently the most widely accepted industry standard among XML-based formats (as opposed to e.g. PDF). Today, EPUB-books are offered by Project Gutenberg, Google Books, iBookstore, Barnes \& Noble and Sony to name a few, and is supported by most notable e-reading devices.

%%%%%%%%%%%%%%%%%%%% NEWSEC %%%%%%%%%%%%%%%%%%
\section{HTML Renderer}

%There are several methods to render HTML in Android. The TextView control supports basic text rendering while WebView is a full-scale WebKit-based web browser component that supports everything from JavaScript and CSS3 to HTML5.

As mentioned in the prior section, EPUB consists primarily of of HTML. At first it was believed that this would save a lot of implementation time due to the availability of a pre-implemented WebKit-based\footnote{WebKit is the rendering engine used in the Chrome and Safari web browsers.} HTML viewer called WebView. Instead, halfway through the project it was found that some features would not be feasible to implement using WebView. 

\subsection{Webview issues with pagination}
The design of the application calls for presenting text in paginated form rather than allowing users to scroll the text. Pagination requires less interaction with the application to read a given length of text. This allows readers to focus on reading rather than interacting with the application. 

In order to divide content into pages the application has to take the size and resolution of the reading area into account, filling it with words until full. Doing this in a WebView requires sending a large amount of data between a source-controlling Java side and the browser-interacting JavaScript side. JavaScript is about three times slower than Java in an Android environment \citep{JavaScriptPerformance}. This performance loss might have been bearable, but the busy interconnect between the two systems was empirically found to be a massive bottleneck that brought the responsiveness of the application down to unacceptable levels. 

\subsection{Webview issues with dedicated views}
One of the main features of our application is dedicated and optimized views for different types of content (see \autoref{subsec:solution:dedicatedview}), such as images and tables. Implementing such views using WebView would require a large amount of Java to JavaScript communication, which was already found
 to be unbearably slow. 


%\subsection{Webview issues with text-selection}
%The question of whether the text should be fully-justified or left-aligned was brought up early in the course of the project. As stated by \cite{Gregory1970}, \cite{Muncer1986} and \cite{Thompson1991}, justification made no difference for good readers but for the poorer readers the fully-justified style resulted in significantly worse reading performance.

%However, other than looking more formal and providing a cleaner look, the major benefit of fully-justified text is that it maximizes word density and fits more text in less space, which in our case minimizes page turns. Since fewer page turns equals less distraction from reading, the application defaults to full justification with the option to disable it.


%Since TextView has limited HTML rendering capabilities and lacks other features such as text selection, it was less suitable. Replacing TextView with an non-editable EditText control solves the text select issue but not the justification. One control that has supports both text selection and justification is the WebView. What the WebView does is basically read a HTML data source and presents that as a web page. If the HTML is styled with justification, the content is justified.

%Using WebView as a final solution soon proved to be impossible as the control renders all HTML content simultaneously and there is no way to determine how much content fits on one screen. This determination is required for pagination processing.
 
\subsection{Implementing a partial HTML-renderer}
Since WebView did not meet the requirements of the application, another solution had to be found. As no existing component was sufficient, a decision was made to implement a new rendering engine that would solve these issues. This new engine was to provide full access to every aspect of the contents of the book.

A complete HTML parse library called jsoup was used to provide programmatic access to the HTML tree structure. This tree was then converted to a linear list form using a standard tree traversal algorithm. 

The linearized HTML is rendered to a canvas on a page by page basis. Rendering text to a canvas requires rather low-level programming but in return provides full control over what’s rendered in terms of color, typeface, font-size, font-spacing, line-height and font-style. This control makes it possible to provide the users with detailed options to change how the text is styled and presented. 

Rendered pages are stored in a slot buffer that keeps a render of the current page and two pages before and after the current one. This reduces perceived render times unless the user is paging very quickly. The renderer renders a regular page on a modern smartphone in about 50 milliseconds.

It should be noted that the renderer cannot render every tag in the HTML specification. It has support for the tags most commonly used in EPUB files, and is intended to evolve and grow as needed.


%A complete HTML parse library called Jsoup is used for this conversion, since it provides a very convenient API for extracting and manipulating data, using the best of CSS, DOM and jquery-like methods.

