\chapter{Result} \label{chap:result}
This chapter presents the prototype that was developed to determine whether the proposed designs were feasible to implement. It also presents some designs that could not be implemented due to time constraints. 

\section{The prototype}
This section contains screenshots and descriptions of the most important components and screens of the prototype. The underlying interaction design principles are described in the second part of the design chapter, and exemplified at the end of this chapter.
 
\subsection{Start screen} 
The start screen is designed with a ``less is more'' approach, presenting the user with a minimalistic screen on application start as seen in \autoref{fig:AppStartScreen}.

Only four menu options - ``My collection'', ``Favourites'', ``Open/Import'' and ``Settings'' - are available on the start screen. This makes it easy for the user to see which actions are available. The buttons, divided by a grid, cover the remaining area of the screen. This makes them very easy to target, minimizing the risk of unintended navigation.

The action bar on this screen only contains a search action which searches through the collection. The search query can be a specific book title, a tag or an author.

Below the start screen action bar is a horizontally scrolling list view of the users recently opened books, arranged in chronological order. This provides quick and easy access to the most recent books, without forcing the user to dig through menus or browse their devices' sd-card. 

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{img/start_screen.png}
    \caption[Application Start Screen]{Start screen.}
    \label{fig:AppStartScreen}
\end{figure}

\subsection{My collection}
The ``My Collection'' activity presents the contents of the collection in three different view fragments (see  \autoref{fig:AppMyCollection}). A fragment is a reusable component or behavioural specification of an Activity. The views are Books, Authors and Tags. The book tab shows all books, the authors tab filters books on a per-author basis, and the tags tab filters books based on user-provided tags. 

The user navigates between the tabs either by a horizontal swipe gesture or by touching the corresponding tab. The application remembers which fragment was last visited and displays it when the activity is relaunched. 

This screen uses a split action bar. The main action bar provides quick access to search and settings, while the bottom bar allows the user to filter and reorder content depending on the current fragment.

Each fragment contains a list view of books, authors or tags. The items in the book fragment can be easily distinguished due to the different book cover art on each row. A small progress bar is shown on top of the cover art, giving the user visual feedback on where they stopped when the book was last read. To allow cutting through menu hierarchies, an action icon is placed to the left on each row. This action icon allows users to invoke special rarely used actions such as ``delete'' and ``details''.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{img/my_collection.png}
    \caption{Book collection screen.}
    \label{fig:AppMyCollection}
\end{figure}

\subsection{Settings} 
This screen presents basic user-adjustable settings, divided into sections to provide a better overview. As shown in \autoref{fig:AppSettings} the appearance of the application adapts according to the operating system version in order to achieve consistency and familiarity.

Settings influencing how the text should be rendered (such as margins and font-size) are provided with a visual feedback element. The visual feedback element is positioned so that thumb occlusion is minimized (see \autoref{fig:AppFontSizeSetting}). The most important of these settings are also accessible from the reader screen, saving users a few navigational steps.

The action bar on this screen provides no other actions then the up-action.

\begin{figure}[H]
    \centering
        \subfigure[Android 3.2 Gingerbread.]{\includegraphics[scale=0.354]{img/settings_ginger.png}\label{fig:AppSettingsOld}}
        \subfigure[Android 4.0 Ice Cream Sandwich.]{\includegraphics[scale=0.354]{img/settings_ics.png}\label{fig:AppSettingsNew}}
        \caption{Settings screen on various platform versions.}
        \label{fig:AppSettings}
\end{figure}

\begin{figure}[H]
    \centering
        \subfigure[Small font size.]{\includegraphics[scale=0.354]{img/settings_font_size_small.png}\label{fig:AppFontSizeSettingSmall}}
        \subfigure[Large font size.]{\includegraphics[scale=0.354]{img/settings_font_size_large.png}\label{fig:AppFontSizeSettingLarge}}
        \caption{Visual feedback for font size setting.}
        \label{fig:AppFontSizeSetting}
\end{figure}

\subsection{Open/Import (file-browser)}
The file-browser activity (see \autoref{fig:AppFileBrowser}) is launched when a user touches the Open/Import button on the start screen. This screen contains a listview populated with the contents of the devices' storage. Icons are used to distinguish directories from files and various file-types. Files that can be read by the application are represented by a special icon. The list is populated first with directories and then with files, with both sections sorted alphabetically. This makes it easier for users to quickly identify their files and e-books.

The contents of the sd-card is displayed at startup, as this is the default user-accessible root directory. Touching one of the list view items invokes different actions depending on file-type:
\begin{description}
\item[Directory] The contents of the directory are displayed, taking the user deeper into the file tree.
\item[Unsupported File] Nothing happens. This list item is disabled and no visual touch feedback is provided in order to demonstrate that this is an unsupported file.
\item[Supported File] The book represented by the file is opened if the file is already present in the collection of the user. Otherwise, a dialogue is displayed. The user can choose between opening the book immediately, importing the book into the collection, or aborting the action. If import is chosen the book is subsequently opened.
\end{description}

If a row in the displayed list represents a directory or a supported file, a checkbox is aligned to the right of the row. A contextual action bar that allows users to import multiple files is displayed when at least one checkbox is checked. The checked subdirectories are recursively scanned for e-books when the action is invoked. All import actions are cancelable, and they display a progress bar which allows users to determine progress.

The bottom of the screen contains a bar displaying the current file path. The main action bar on this screen has a parent directory action, which allows users to change the directory displayed to the parent of the current directory. The ``back'' button functionality is overridden in this activity. Here it invokes the parent directory action, unless the current directory is the root directory. 

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{img/file_browser.png}
    \caption{File browser screen.}
    \label{fig:AppFileBrowser}
\end{figure}

\subsection{Read screen}
The read activity is the most important screen of the application, as this is where the users read their e-books. Users are presented with a full-screen representation of a book page when the activity is launched. The first page of the book is displayed if it's the first time the book is read, otherwise the last seen page is displayed. 

The page is actually a rendered bitmap of the current content in the e-book file (See ~\nameref{chap:Implementation}). As  \autoref{fig:AppRead} shows, no action bar is displayed and the Android system notification bar is hidden. The bar is hidden to provide less distraction from the reading experience and to give more room for content, which according to \cite{Tidwell2005} helps create a sense of flow. 

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{img/book_view.png}
    \caption{Read screen.}
    \label{fig:AppRead}
\end{figure}

To navigate within this screen and invoke different actions, some basic gestures are used. These gestures all practise our "forgiving navigation" concept:

\begin{description}
\item[Single tap in center of the screen] Displays the ``read overlay'' view, the system notification bar and the action bar. The center of the screen is defined as the vertical box that is in the center-most 33\% of the horizontal space available.
\item[Swipe left/right] Changes the page. The sensitivity of the gesture depends on how fast you are swiping; slow swipes require you to swipe over at least 50\% of the width of the screen, in order prevent unintentional page changes. Left and right sides of the screen are defined by the vertical box that is in the right- or left-most 33\% of the horizontal space available. The vertical accuracy of the swipe is unimportant as single-finger vertical gestures have intentionally been left unused.
\item[Tap on left/right side of the screen] Changes the page. Users are required to touch and release in the same area to prevent unintentional page changes.
\end{description}

It is also possible to use the hardware volume keys to change pages; volume up switches to the next page while volume down switches to the previous page. 

\subsection{Reading overlay}
\label{readingOverlay}
The read overlay has a black background with 30\% transparency to help users feel that they are still on the same page in the book. This view also features a \emph{chapter scroller} on the bottom of the screen. This navigational scroll view allows users to easily scroll into different hierarchical sections of the book. 

Figure \ref{fig:AppReadOverlayCollapsed} shows the scroll view in its collapsed state. Dragging the handlebar creates new instances of the scroll view underneath the top level scroller. These new scrollers are are populated with items based on the structural children of the selected item in the bottom-most scrollview. For example, dragging the handlebar of a chapter will produce a scroller with the sections making up that chapter. Further dragging of a section displays subsections (if available), and so on. The first page of the chapter, section or page is shown when the corresponding box is clicked. 

Figure \ref{fig:AppReadOverlayExpanded} shows the controls that allow the user to navigate between chapters, parts within the selected chapter and pages within the selected part on the same screen.

The multi-level structure is designed to make it easy to drill down to a specific part of the document. It enables the user to quickly go back and forth between different sections of the book once the relative distance between the sections of interest is known, as a single fling can scroll between 10 and 60 chapters in a very short time. If the user over- or undershoots the target it's easy to correct the mistake with smaller swipes.

\begin{figure}[H]
    \centering
        \subfigure[Collapsed navigation scroll view.]{\includegraphics[scale=0.354]{img/book_overlay_collapsed.png}\label{fig:AppReadOverlayCollapsed}}
        \subfigure[Expanded navigation scroll view.]{\includegraphics[scale=0.354]{img/book_overlay_expanded.png}\label{fig:AppReadOverlayExpanded}}
        \caption{Overlay view.}
        \label{fig:AppReadOverlay}
\end{figure}

The action bar contains the bookmark and search functions when the overlay is open. Choosing which actions to expose here is an important decision, as this action bar likely is the one that will see the most use. These two actions were chosen over other important functionality such as text-resizing and day/night-mode, based on the expectation that users would more often like to use search and bookmarking. Users can still access such functionality through the action overflow icon. 

\subsection{Dedicated views}
\label{subsec:DedicatedViews}
Dedicated views for non-text content is an important feature of the application. Due to time constraints, only the image view was implemented. While fairly simple, it is still a good demonstration of the concept. 

Placing the image in its own view rather than as a part of the text view allows users to see the entire image at their magnification of choice. This makes complex diagrams or large illustrations usable for mobile users.  

The design of this image view also exemplifies part of the forgiving navigation concept. It provides a thumb-friendly alternative to the common multi-touch gesture ``pinch-to-zoom''. A sliding zoom control is displayed when a user touches the images, and fades away after use. Multi-touch is still available, but the redundant controls give the users greater flexibility without being overly distracting. This design is fully inclusive of those who only use one thumb.

Images that fit entirely inside the reading area do not need a special view and are simply shown as-is. Larger images are provided with a wrapper around a cropped thumbnail of the image. This wrapper, as shown in  \autoref{fig:AppReadImage} has a distinct trigger button. This helps differentiate between the two cases, and also aids user discovery of the full-screen functionality.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{img/book_image.png}
    \caption{Image object with clickable wrapper.}
    \label{fig:AppReadImage}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{img/book_image_view_zoom.png}
    \caption{Image view with thumb-friendly zoom control visible.}
    \label{fig:AppImageView}
\end{figure}

%%%%%%%%%%%%%% NEW SECTION YAYYY %%%%%%%%%
\section{Un-implemented designs}

This section covers some design concepts which were left unimplemented due to time constrains. 

\subsection{Text select dialogue}
\label{unimplemented:textselect}
One unimplemented design is the interactive text select dialogue shown in \autoref{fig:AppTextDialogue}. Users can select text by performing a long-press on any text element in the read activity. The pressed word changes background color to indicate its selected state and two draggable ``handlebars'' are displayed to the left and right of the selected word. These bars can be dragged to change what is selected. Once the user taps the range of text the text select dialogue is displayed.

This dialogue mimics the design of the convenient ``contact card dialogue'' known in Android 4.0 but provides different functionality. Different tabs can be selected for various features as in the contact card dialogue. The figure illustrates real-time search results from Google since the ``G'' tab is selected. Wikipedia results and word definitions are similarly displayed while the ``share'' tab provides sharing options. A tab label is also provided since the icon metaphors are not completely self-explanatory and could have multiple meanings.

The dialogue provides shortcuts to actions that would otherwise require the user to launch several other applications. The dialogue thereby helps the users to maintain focus on reading since they stay within the application. For example, if a user touches a Google result from the list, the browser immediately displays that result and a simple back press takes the user back to the reading application.

\begin{figure}[H]
    \centering
        \subfigure[3Book text interaction dialogue.]{\includegraphics[scale=0.354]{img/selected_text_interaction.png}\label{fig:AppTextDialogue}}
        \subfigure[Android 4.0 contact card.]{\includegraphics[scale=0.354]{img/contact_card.png}\label{fig:AppContactCard}}
        \caption{Interactive features with a selected text compared with legacy contact card. From left to right: Google search, wikipedia search, in-book search and sharing options.}
        \label{fig:AppDialogue}
\end{figure}

\subsection{Dedicated table view}

\autoref{fig:AppTableView} illustrates another unimplemented design. Like the dedicated image view, this view is optimized for one type of content: tabular data.

Tables are shown in the reading view as a thumbnail surrounded by a wrapper with a distinct ``view'' button, similar to how large images are presented. Touching the button launches the table view, which displays the data in a zoomed out state, as shown in  \autoref{fig:AppTableViewOut}.

Zooming the data can be done with a pinch-to-zoom gesture or by using the zoom controls, which are located in the bottom of the screen to maintain consistency with the rest of the application. Each individual row and column are resizable via the ``handlebars'' that are set in the space between two such elements. Double-tapping any header item or row number adjusts the cell size to fit the content entirely. A swiping gesture over the cells will pan the data viewport while header items and row numbers remain locked in position.

Rows and columns can be long-pressed to bring up a menu which offers the user the option of moving or hiding the row or column. A ``hidden'' row or column still takes up a few pixels of real estate which the user can zoom in and long press to display a menu option to restore the column to its previous size.

Tapping a column header causes the application to attempt to sort the table in ascending order based on the content of that column. A second tap reverses the sort order while the third tap brings the table back to its unsorted state. 

This specialized table view provides more interactivity compared to a standard HTML table. The resizing, moving and hiding options let the user configure the table to focus on the parts of the table which interest them. These options, together with the zoom feature, also help reduce the need for horizontal scrolling, which has been found to greatly increase user seek time and reduce usability \citep{Kim2003}.    

\begin{figure}[H]
    \centering
        \subfigure[Zoomed in table.]{\includegraphics[scale=0.354]{img/table_view_zoomed_in.png}\label{fig:AppTableViewIn}}
        \subfigure[Zoomed out table.]{\includegraphics[scale=0.354]{img/table_view_zoomed_out.png}\label{fig:AppTableViewOut}}
        \caption{Tabel data in dedicated view.}
        \label{fig:AppTableView}
\end{figure}


\section{Forgiving navigation examples}
One of the chief principles developed during the project has been the notion of forgiving navigation discussed in \autoref{subsec:forgivingNavigation}. Being an extension of \citepos{Tidwell2005} ``safe exploration'', forgiving navigation can and has been applied in all parts of the application.  

One example of how forgiving navigation has been considered in 3Book is how progressing through the book is done. On the read screen, users are limited to progressing by pagefulls of content through touching the edge of the screen or swiping across it. Scrolling, such as when reading a web page, is disabled since this interaction promotes frequent or continuous scrolling while reading. Instead the application itself determines how much content fits on the screen presents this, requiring the least amount of cognitive effort from the reader. This makes for a transient page-flipping experience which helps the reader to focus on the content.

Disabling vertical scrolling also hold the merit that vertical movements during swiping can be ignored. This means a swipe doesn't have to be perfectly horizontal to work and making page-flipping less error prone. In contrast, the popular book-reading application Moonreader+ supports both paging and scrolling of content, but an inexact page-flip swipe can be interpreted as a page scroll action, with unexpected results.

Another forgiving navigation method is the chapter scroller described in the section ``\nameref{readingOverlay}''. The user is free to browse through the different chapters without any change to the current reading position. If the correct chapter or section is found a jump to the new position is achieved through clicking it's box in the scroller. 

The boxes representing a position within the book are all very large (each side is longer than 15 mm), which makes misclicks unlikely \citep{Park2010, Parhi2006}. Misclicks that still occur are easily corrected, as very few items are shown at a time. The user can simply bring up the scroller with another tap and re-select the correct item, which is likely adjacent.

In order to expand a chapter into a list of sections the chapter's box has to be at the center of the scroller. Positioning the box correctly can be finicky when done manually, so the center-most button is automatically positioned in the middle of the scroller once the user stops touching the screen. The user can then either click the box to display the chapter, or drag the handlebar upwards in order to explore the sections of the chapter. The same goes for the expanded scroller, where clicking takes the user to the section and dragging exposes sub-sections or pages.

The greatest downside of the design of the chapter scroller is the rather low information density, which makes it more difficult to get a wide overview of the breadth of content at a certain level. This is a problem for users that are not acquainted with the overall structure of the book. For this reason, a traditional ``top-down'' table of contents is available on a separate screen, which presents a wide, rather than a deep, view of the contents. 

In contrast, Aldiko (a popular e-book reader), uses a ``seekbar'' component to navigate within a book (See \autoref{fig:AldikoRead}). This works rather well for short books, but a large number of pages requires extreme precision on the part of the user, as only a few pixels of movement could result in jumping an entire chapter.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{img/aldiko_read.png}
    \caption{Aldiko book reader application showing overlay.}
    \label{fig:AldikoRead}
\end{figure}

