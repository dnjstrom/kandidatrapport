\section{Solving the problem by design} \label{sec:solveByDesign}

The solution to the smartphone problem is good interface design. This section presents our solutions to the problems outlined in the previous section.  

\subsection{Thumb-driven interface}
As noted in \autoref{p:thumbdriven}, users often prefer to interact with their devices using a single hand and its thumb. Ensuring that all of the functionality is usable with a single thumb, provides benefits beyond catering to the preferences of the users. It opens up new usage scenarios for situations when the other hand is otherwise occupied, and creates a level playing field for those with medical conditions that preclude the use of two hands. 

Ensuring thumb-only access can be seen as a mobile variant of the ``Keyboard Only'' pattern suggested by \cite{Tidwell2005}. It is important to note that designing for the thumb does not imply that multi-touch gestures are prohibited; rather, it means that there should always be a thumb-only alternative available. One such example is providing zoom in/out buttons for image views, while still retaining the standard pinch-to-zoom gesture in the view. 

Designers concerned about cluttering their interfaces with ``unnecessary'' controls could provide a menu option that hides the thumb-helping interface elements. This allows the users to make choices about which interface is best for them, instead of prescribing a solution that might not be a good fit for everyone. 

\subsection{Forgiving navigation} 
\label{subsec:forgivingNavigation}
Forgiving navigation is a design concept that was introduced to deal with low-precision touch controls and the cramped nature of the small screen. It is all too easy for the user to make mistakes even with generous control sizes. Designers should pay attention to aspects of the interface that demand high precision from the user as well as interactions that exact a high price when a mistake is made. 

The user is not at fault for making click mistakes or misunderstanding the purpose of the interface. Smartphones are a fairly new class of devices with a novel interaction model. Over 850000 devices are activated each day in the Android ecosystem alone \citep{Rubin2012}, which means that a lot of users are experiencing touch interaction for the first time. Smartphones are also frequently used ``on-the-go'' or in situations when the user cannot fully concentrate on their interaction with the device. Such circumstances makes users more error prone, meaning they could benefit from forgiving designs.

\subsection{Content-specific views}
\label{subsec:solution:dedicatedview}
The lack of screen real estate makes it difficult to place content and controls on the same screen. A single modern e-book can contain several different types of content such as text, images, tables, and video. The user interacts with these diverse kinds of contents in different ways, and needs unique controls for each. This is difficult to provide using only touch input, as the small screen size restricts how many interface elements can be placed.

By shifting each type of content to its own specific view both screen and gesture space is freed up. This allows rich interaction tailored to the specific content that is shown. Video and audio content can use on-screen swipes to fast-forward or rewind, while images and tables can be panned with the same action. Placing thumb-friendly interface elements is easier, as screen space is freed up by removing controls for other types of content.

%the text view shows text and nothing else; all interface elements are hidden until the user summons them by a simple tap in the middle of the screen or by clicking the menu button. text is always re-flown, re-formatted and re-styled to better fit the small screen. the user can set various styling options (colour of text / background, size of text, margins, etc) to better fit her preferences.

%images are shown in thumbnail form in text view. when a user clicks a thumbnail a new view takes over the screen and uses the entire available space to show the image. should this not be enough (as is often the case) the user can pan and zoom the image as she pleases. connecting with the forgiving navigation concept, we offer pinch-to-zoom as well as single-digit controls for zooming the image.

%tables are shown in specialized pannable views, also accessible by thumbnails. if the application recognizes the type of information in each column it is sortable by a simple click. the user can hide and/or resize columns on the fly to enable her to focus on only the parts of the table that she's interested in. rows can be deleted too. 

%video is shown in fullscreen accessible by thumbnails. so is audio (and it would be neat to have the audio play in the background while reading, if we can come up with a good control for it)


\subsection{Advanced navigation}

Textbooks are increasingly often available as e-books, and while the e-readers of today are fairly good at displaying novels, textbooks require a device with different capabilities. Reading a textbook is much more of a dynamic exercise than a linear progression. Often different sections of the book is read in parallel or cross-referenced. Footnotes and references introduces the need to jump to a certain point in the text and then quickly return. Certain parts of the book might be desirable to quickly jump to at any point in reading. These examples highlight the need for excellent and multi-faceted navigation options.

Textbooks are often long and well-structured, with several hierarchical levels of structure. This structure should be made easily accessible to the reader in such a way as to allow the reader to get a clear overview of the anatomy of the book. Such an overview permits quickly moving about in the text and supports a dynamic way of reading. Efforts should be made to minimize the amount of loading required to switch between different parts of the book with the goal of removing any time penalties.  

%Bookmarks, a common idiom of traditional books, should be supported as to enable quick navigation to commonly visited places in the book. The user controls where to place bookmarks and all bookmarks should be easily accessible from any point in the text.

A crucial part of the navigation design consists of the proper use of the back-button. After having looked up a reference or cross-referenced a different chapter, an easy method of return should be provided to the user. Keeping a history of navigations in memory, a simple touch of the back hardware key should bring the reader to the original position in the text. Only major navigation actions, such as jumping directly to a chapter or following a hyperlink, should be remembered. This allows a user to jump to a position in the vicinity of the target section, explore the surrounding content using the standard flip-page forward/backward gestures, and when satisfied return to the original position.  

In a scholarly context, one book is seldom used on its own. A textbook about physics might be used in conjunction with a reference book. In such a case, easy and quick switching between the different books is of vital importance. Each book should have its own navigation history and switching between several open books should be quick and effortless.
